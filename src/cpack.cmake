## Common CPack variables for project

set(MAJOR_VERSION "1")
set(MINOR_VERSION "0")
set(PATCH_VERSION "0")
set(CPACK_PACKAGE_VERSION ${MAJOR_VERSION}.${MINOR_VERSION}.${PATCH_VERSION})

set(CPACK_PACKAGING_INSTALL_PREFIX ${CMAKE_INSTALL_PREFIX})
set(CPACK_GENERATOR "DEB")
set(CPACK_PACKAGE_NAME "dku_dev_shmem")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "This is the core of a runtime environment that manages multiple threads (contexts) in user space")
set(CPACK_PACKAGE_VENDOR "Sergey Gvozdetskiy")
set(CPACK_PACKAGE_CONTACT "sgvozdetskiy@yahoo.com")
set(CPACK_DEBIAN_PACKAGE_HOMEPAGE "http://opensourceresearchinstitute.org/pmwiki.php/PRT/HomePage")
set(CPACK_DEBIAN_PACKAGE_SECTION "devel")
set(CPACK_DEBIAN_PACKAGE_PRIORITY "optional")
set(CPACK_COMPONENTS_ALL test_app_dku lib_pr lib_pr-dev lib_util_c lib_util_c-dev)
set(CPACK_DEB_COMPONENT_INSTALL ON)
set(CPACK_PACKAGE_FILE_NAME "${CPACK_PACKAGE_NAME}-${CMAKE_SYSTEM_PROCESSOR}")
