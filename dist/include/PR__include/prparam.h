/* 
 * 
 * Author: SeanHalle@yahoo.com
 *
 * Created on November 19, 2009, 6:30 PM
 */

#ifndef _PARAM_H
#define	_PARAM_H

#include <PR__include/PR__primitive_data_types.h>


typedef
struct
 { int type;
   int intValue;
   char * strValue;
   float floatValue;
 }
ParamStruc;

#define INT_PARAM_TYPE    0
#define STRING_PARAM_TYPE 1
#define FLOAT_PARAM_TYPE  2

#define PARAM_BAG_HASHSIZE 1024

typedef struct _ParamBagHashEntry ParamBagHashEntry;

struct _ParamBagHashEntry
 {
   char       *key;
   ParamStruc *param;
   struct _ParamBagHashEntry *next;
 }
/*ParamBagHashEntry*/;


typedef
struct
 { int bagSz;
   ParamBagHashEntry* *entries;
 }
ParamBag;


ParamBag    *makeParamBag();
void         readParamFileIntoBag( char *paramFileName, ParamBag * bag );
ParamStruc  *getParamFromBag( char *key, ParamBag * bag );
int          addParamToBag( char* key, ParamStruc *param, ParamBag *bag );
void         freeParamBag( ParamBag *bag );
//char        *paramBagToString( ParamBag * bag );
ParamStruc  *makeParamStruc();
ParamStruc  *makeParamFromStrs( char * type, char *value );
ssize_t      getline( char **lineptr, size_t *n, FILE *stream );

#endif	/* _PARAM_H */

