
This directory is a copy of /usr/include/PR__include

It holds the header files that are public, for all the independent libraries created, related to proto-runtime (as of now, just the universal version).

In particular, the individual repositories do NOT contain these header files!  If any of them change these, then the /usr/include/PR__include directory has to be copied to the windows shadow copy, then committed to the repository.

