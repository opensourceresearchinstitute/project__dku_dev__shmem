To just build everything and skip the explanations, type:
$ make
$ RUNME.sh

You need cmake to be installed.

If it doesn't work, then chances are that the submodules have not been
recursively cloned.  To get them type:

$ git submodule init
$ git submodule update --recursive

To run applications stand alone, the LD_LIBRARY_PATH variable has to be extended to include a PR__lib with the correct library versions in it, as shown inside RUNME.sh


===
= If you're interested in development, read on..
===

First things first, there are two separate development contexts --
development context, and debian packages context.  Due to complex interactions among
the pieces, it is more convenient to develop within a rather unusual
arrangement of the code, as explained below.  The code is arranged this way
in the master branch.  However, once development is done then it is time to
create debian packages.  This requires the code be treated as many
independent stand alone environments.  So each repository has a separate
"debian" branch.  In this branch the cmake is different, and in some places
paths in the code are different. 

========
This directory holds a number of separate, related, projects.  Each is built independently.  They have all been originally developed in netbeans, which auto-generates the makefiles.  The netbeans project directories are included.

There is also a separate set of cmake files to build this top level project.

The wiki has more information about the organization of projects related to proto-runtime, getting netbeans, and so on:  
http://opensourceresearchinstitute.org/pmwiki.php/PRT/ProjectDownloads

(Please note that this is a rapidly changing research environment, so this file may be old, and not reflect the current circumstances of the project..  please understand, there are many, many details, and only a few people doing them all)

==========  The directories and sub projects  ==========

the dist/ directory holds the include files, the built library files, and the built application executables, as well as any data required as input to the applications, and output produced by them.  This is just a convenient configuration during development of the proto-runtime toolkit itself.  This directory is a convenient organization for such development.

There are three sub projects.
One is the application, which makes calls to the language's runtime system. It is linked to both the language runtime library and the proto-runtime library. The built libraries are in dist/lib.  The built application is in dist/bin.  The application project will be inside src/applications

Another sub project is the language runtime implementation, which makes calls to the proto-runtime primitives.  It is a directory in src that has the pattern src/library__<name>  

The third is the proto-runtime toolkit library.  
It contains implementations of proto-runtime primitive operations.  These operations are meant to be used by plugins, the API interface library (AKA wrapper library), and a few directly from the application code.  The application code calls are used to, for example, start up the proto-runtime system.  The wrapper library calls commands such as "suspend and send".  This project is in src/library__proto-runtime

All of these use include files located in dist/include
And all of them are configured to look for library .a and .so files in dist/lib


==========  Organization of a sub project directory  ===========

Each sub project has its own directory, which contains:
-] the source code
-] some text files containing design notes (may not be very helpful, used during development as an aid to design and write the code)
-] a ".hg" folder for the mercurial repository
-] a .git for the git repo
-] one or two folders starting with "nb_" that hold the netbeans project information for that sub-project

For a shared library project, there are two netbeans folders.  One is for developing the library, and the project includes some simple test application that exercises the library, in order to debug it.  The second project builds the library file and moves it into the PR__lib directory.

==========  Building the projects without netbeans  ===========

There is a cmake build included.  To build, go to root of this project, where this file is located, and type:
make

That will invoke the cmake system, and generate a directory in which all the build activity takes place.

The application executable will be placed into  dist/bin/

The library files (.so or .a) will be put into dist/lib

===========  Developing the code without netbeans  ============

To develop without netbeans, note that the header files are in dist/include/PR__include and the libraries are in dist/lib.  To run the built application, the "LD_LIBRARY_PATH" has to have dist/lib added to the end "$LD_LIBRARY_PATH:./dist/lib"..  that works when the executable is run from this root directory


==========  Developing the code in Netbeans  =============

The Netbeans projects were the original way that the code was developed, but some of the netbeans project directories  may no longer be synchronized with the current state of the code.

Each sub-project has a directory that starts with "nb__", which contains the netbeans project that will build that project.

You can just open an existing one, or create your own.

To create a netbeans project:

-] For creating an application, choose a "C/C++ Application" as the project type (uncheck "Main.cpp")..  place the project into the same directory as the top level of the source code, and give it a name that starts with "nb__".

-] Once created, then from the "projects" pane, expand the project, then right click on "Source Files", select "Add existing item", navigate to the application directory and choose "main.c" and any other files (such as "PR_defs__turn_on_and_off.h")

-] Then select "Add existing items from folder" and navigate to the directories that contain proto-runtime language code.  
For example, for PRDSL project, navigate to PRDSL__Test_App and select the "PRDSL__Test_App" within it (both directories named the same)

Now, set the include directory:
-] right click on project, select "Properties" then in the pop-up select the "C Compiler"  find "Include Directories" and click the "..." next to it.  navigate to the directory just _above_ "PR__include" -- which is the top level dir of the project and select it.  Be sure that "relative" path radio button is selected.

The language and proto-runtime code is in libraries, so add them:

-] right click on project, select "Properties" then in the pop-up select the "linker" bullet

-] Find "Additional Library Directories", and next to it click on "..." and navigate to the PR__lib directory that is in the top directory (the one that uncompressed from the zip file).  Select that, to add it as an additional library directory.

-] Next to the "libraries", click on the "..." button to bring up a new dialog box

-] add the pthreads and math libraries by clicking on the "standard include libraries" button and choosing pthreads and mathematics.

-] add the rest of the libraries by choosing "add library", which should show the PR__lib directory that was added above

Each project needs a different set of libraries added.

If building an application that uses one of the languages, then the only libraries required are:

libpr__ml__sharedmem.so
lib<language wrapper lib>

Note that the C_libraries directory contains many helpful libraries commonly used in proto-runtime projects.

-] Now set where the final executable or library is to be placed.  Still under "Properties->Linker" find "Output" and click on "..." then edit the line.  For an application, target the dist/bin directory that's in the top-level dir, and give the desired name of the executable.  For a library, target the PR__lib directory and name it "lib<name>.so".  Note, that for the netbeans project that comes with the .zip file, that is for the application, the name it uses for the output is also encoded in the RUNME.sh file..

======================================================

To run applications stand alone, the LD_LIBRARY_PATH variable has to be extended to include a PR__lib with the correct library versions in it, as shown inside RUNME.sh

Note that older versions of netbeans have a bug, where it includes "-fPIC" as argument to the assembler, for assembly files included in dynamic library projects.  In this case, have to manually modify the generated makefile (in nbproject/Makefile-Debug.mk) and remove the "-fPIC" from lines starting with "$(AS)", then run make by hand.  It should automatically put the generated library into the correct position in the PR__lib directory..

To be able to debug from within netbeans, set up the run properties:
-] Select project and right-click, choose "properties"
-] click on "Run"
-] find "Run Directory" and click on "..." navigate to the top level directory
-] find "Environment" and click "..." enter a new environment variable "LD_LIBRARY_PATH" with value "$LD_LIBRARY_PATH:./PR__lib"

Notice that this is the same run directory as the RUNME.sh file, and the same LD_LIBRARY_PATH setting..

