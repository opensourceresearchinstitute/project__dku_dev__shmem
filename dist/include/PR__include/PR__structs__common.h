/*
 *  Copyright 2009 OpenSourceResearchInstitute.org
 *  Licensed under GNU General Public License version 2
 *
 * Author: seanhalle@yahoo.com
 * 
 */

#ifndef _PR__structs__common_H
#define	_PR__structs__common_H
#define _GNU_SOURCE

#include <pthread.h>
#include <sys/time.h>

//#include "PR_defs__turn_on_and_off.h"
#include <PR__include/PR__primitive_data_types.h>
#include <PR__include/Services_offered_by_PR/DEBUG__macros.h>
#include <PR__include/Services_offered_by_PR/MEAS__macros.h>

#include <PR__include/prhistogram.h>     //reqd by PRProcess
#include <PR__include/prhash.h>   //reqd by PRProcess
#include <PR__include/prdynarray.h>       //reqd by PRProcess
#include <PR__include/prqueue.h> //reqd by PRLangEnv, in turn reqd by PRProcess

//================================ Typedefs =================================
//===
//=
#define ZERO 0

//typedef unsigned long long    TSCount;

typedef struct _AnimSlot      AnimSlot;
typedef struct _PRReqst       PRReqst;
typedef struct _SlaveVP       SlaveVP;
//typedef struct _MasterVP      MasterVP;
typedef struct _IntervalProbe IntervalProbe;
typedef struct _PRLangEnv     PRLangEnv;  //a prolog
typedef struct _PRMetaTask    PRMetaTask; //a prolog
typedef struct _PRLangData    PRLangData; //a prolog
typedef struct _PRCollElem    PRCollElem; //generic form of the prologs

typedef bool32   (*SlaveAssigner)  ( void *, AnimSlot* ); //langEnv, slot for HW info
typedef void     (*RequestHandler) ( void *, SlaveVP *, void * ); //req, slv, langEnv
typedef void    *(*CreateHandler)  ( void *, SlaveVP *, void * ); //req, slv, langEnv
typedef void     (*LangShutdownHdlr) ( void * ); //langEnv
typedef void    *(*LangDataCreator)  ( SlaveVP * ); 
typedef void     (*LangDataFreer)    ( void * ); //lang data to free
typedef void    *(*LangMetaTaskCreator)( SlaveVP * );  //when slave has no meta task for magic num 
typedef void     (*LangMetaTaskFreer)  ( void * ); //lang meta task to free
typedef void     (*MakeSlaveReadyFn)   ( SlaveVP *, void * ); //slave and langEnv
typedef void     (*MakeTaskReadyFn)    ( void *, void * ); //langTask and langEnv
typedef void     (*BirthFnPtr)  ( void *, SlaveVP * ); //initData, animSlv
typedef void       BirthFn      ( void *, SlaveVP * ); //initData, animSlv
typedef void     (*ResumeSlvFnPtr) ( SlaveVP *, void * );
      //=========== MEASUREMENT STUFF ==========
        MEAS__Insert_Counter_Handler
      //========================================


typedef struct
 { //These are set by the plugin during startup and the application
   char *assignerInfo;
   char *appInfo;
   char *inputInfo;
 }
PRSysMetaInfo;

//=====================  Process Data Struct  ======================

/*This structure holds all the information PR needs to manage a program.  PR
 * stores information about what percent of CPU time the program is getting, 
 * 
 */

typedef struct
 { 
   int32     numEnvsWithWork;
   void     *resultToReturn;
    
   PRLangEnv **langEnvs;     //used as a hash table
   PRLangEnv **protoLangEnvsList; //for fast linear scan of envs
   int32       numLangEnvs;   //for fast linear scan of envs
    
   SlaveVP        *seedSlv;
   
   int32           numLiveGenericSlvs;
   int32           numLiveTasks;

   SlaveAssigner   overrideAssigner;
  
   int32           numConsecutiveTasks;
   
   
      //These are used to coord with an OS thread waiting for process to end
   bool32          hasWaitingToEnd;
   bool32          executionIsComplete;
   pthread_mutex_t doneLock;
   pthread_cond_t  doneCond;   
   pthread_mutex_t doneAckLock; //waiter gets, then releases when done waiting

      //=========== MEASUREMENT STUFF =============
       IntervalProbe   **intervalProbes;
       PrivDynArrayInfo *dynIntervalProbesInfo;
       HashTable        *probeNameHashTbl;
       int32             masterCreateProbeID;
       float64           createPtInSecs; //real-clock time PR initialized
       Histogram       **measHists;
       PrivDynArrayInfo *measHistsInfo;
       MEAS__Insert_Susp_Meas_Fields_into_MasterEnv;
       MEAS__Insert_Master_Meas_Fields_into_MasterEnv;
       MEAS__Insert_Master_Lock_Meas_Fields_into_MasterEnv;
       MEAS__Insert_Malloc_Meas_Fields_into_MasterEnv;
       MEAS__Insert_Plugin_Meas_Fields_into_MasterEnv;
       MEAS__Insert_System_Meas_Fields_into_MasterEnv;
       MEAS__Insert_Counter_Meas_Fields_into_MasterEnv;
      //==========================================
 }
PRProcess;


//============= Request Related ===========
//

enum PRReqstType  //avoid starting enums at 0, for debug reasons
 {
   TaskCreate = 1,
   TaskEnd,
   SlvCreate,
   SlvDissipate,
   Language,
   Service,       //To invoke a PR provided equivalent of a language request (ex: probe)
   Hardware,
   IO,
   OSCall, 
   LangShutdown,
   ProcessEnd,
   PRShutdown
 };


struct _PRReqst
 {
   enum PRReqstType   reqType;//used for special forms that have PR behavior
   void              *langReq;
   PRProcess         *processReqIsIn;
   int32              langMagicNumber;
   SlaveVP           *requestingSlave;
   
   BirthFnPtr         birthFn;
   void              *initData;
   int32             *ID;
   
      //The request handling structure is a bit messy..  for special forms, 
      // such as create and dissipate, the language inserts pointer to handler
      // fn directly into the request..  might change to this for all requests
   RequestHandler    handler; //pointer to handler fn 
   CreateHandler     createHdlr; //special because returns something
   int32             createSuspendedGroup; //must be non-zero
   
   PRReqst *nextReqst;
 };
//PRReqst

enum PRServiceReqType   //These are equivalent to lang requests, but for
 {                    // PR's services available directly to app, like OS
   make_probe = 1,    // and probe services -- like a PR-wide built-in lang
   throw_excp,
   openFile,
   otherIO
 };

typedef struct
 { enum PRServiceReqType   reqType;
   SlaveVP             *requestingSlv;
   char                *nameStr;  //for create probe
   char                *msgStr;   //for exception
   void                *exceptionData;
 }
PRServiceReq;


//====================  Core data structures  ===================

typedef struct
 {
   //for future expansion
 }
SlotPerfInfo;

struct _AnimSlot
 {
   int32         workIsDone;
   int32         needsWorkAssigned;
   SlaveVP      *slaveAssignedToSlot;
   
//   int32         slotIdx;  //needed by Holistic Model's data gathering
   int32         coreSlotIsOn;
   SlotPerfInfo *perfInfo; //used by assigner to pick best slave for core
 };
//AnimSlot

enum VPtype 
 { SlotTaskSlv = 1,//Slave tied to an anim slot, only animates tasks
   FreeTaskSlv,   //When a suspended task ends, the slave becomes this
   GenericSlv,     //the VP is explicitly seen in the app code, or task suspends
   SeedSlv,
   Master_VP,
   ShutdownVP,
   IdleVP
 };
 
/*This structure embodies the state of a slaveVP.  It is reused for masterVP
 * and shutdownVPs.
 */
struct _SlaveVP
 {    //The offsets of these fields are hard-coded into assembly
   void       *stackPtr;         //save the core's stack ptr when suspend
   void       *framePtr;         //save core's frame ptr when suspend
   void       *resumeInstrPtr;   //save core's program-counter when suspend
   void       *coreCtlrFramePtr; //restore before jmp back to core controller
   void       *coreCtlrStackPtr; //restore before jmp back to core controller
   
      //============ below this, no fields are used in asm =============
   
   void       *startOfStack;  //used to free, and to point slave to Fn
   PRProcess  *processSlaveIsIn;
   enum VPtype typeOfVP;      //Slave vs Master vs Shutdown..
   int32       slaveNum;      //each slave given it's seq in creation
   int32      *ID;       //App defines meaning of each int in array
   int32       coreAnimatedBy; 
   int32       numTimesAssignedToASlot;   //Each assign is for one work-unit, so is an ID
      //note, a scheduling decision is uniquely identified by the triple:
      // <slaveNum, coreAnimatedBy, numTimesAssignedToASlot> -- used in record & replay
   
      //for comm -- between master and coreCtlr & btwn wrapper lib and plugin
   AnimSlot   *animSlotAssignedTo;
   PRReqst    *request;      //wrapper lib puts in requests, plugin takes out
   void       *dataRetFromReq;//Return vals from plugin to Wrapper Lib

      //For language specific data that needs to be in the slave
      //These are accessed  directly for single-lang, but multi-lang places
      // a holder here instead, then uses magic num to get lang's version
   PRLangData  **langDatas;  //Lang saves lang-specific things in slave here
   PRMetaTask  **metaTasks;

//   PRGhostInfo  *ghostInfo;
   
        //=========== MEASUREMENT STUFF ==========
         MEAS__Insert_Meas_Fields_into_Slave;
         float64     createPtInSecs;  //time VP created, in seconds
        //========================================
         
//   int8       cacheLinePad[512 - sizeof(contents)]; //for false sharing
 };
//SlaveVP

 
enum PRMode
 { SingleLang = 1,
   StandaloneWTasks,
   MultiLang      
 };
 

//===================== These are prologs ====================
//===A prolog is data immediately before pointer returned by a create function.
//=
struct _PRLangEnv
 { //============== First two must match PRCollElem ==============
   int32     langMagicNumber; //indexes into hash array of langEnvs in PRProcess
   PRLangEnv *chainedLangEnv;   //chains to langEnvs with same hash
   //=============================================================
   
   SlaveAssigner    workAssigner;
   LangShutdownHdlr shutdownHdlr; //called when lang ended or process shutdown
   LangDataCreator  langDataCreator;
   LangMetaTaskCreator langMetaTaskCreator;
   MakeSlaveReadyFn makeSlaveReadyFn;
   MakeTaskReadyFn  makeTaskReadyFn;
     
      //when multi-lang, master polls lang env's to find one with work in it..
      // in single-lang case, flag ignored, master always asks lang for work
   int32           hasWork;
   PRProcess      *processEnvIsIn;
   
   int32           idxInProcess; //index into array of langEnvs in the process
   
   int32           numReadyWork;
   
   int32           numLiveWork;
   PrivQueueStruc *waitingForWorkToEndQ;
 };
//PRLangEnv -- this is the prolog of every lang's lang env

enum PRTaskType
 { GenericSlave = 1,
   SlotTask,
   FreeTask
 };

struct _PRMetaTask
 { //============== First two must match PRCollElem ==============
   int32           langMagicNumber;
   PRMetaTask     *chainedMetaTask;
   //=============================================================
   enum PRTaskType taskType;
   int32          *ID;              //is standard PR ID
   PRProcess      *processTaskIsIn;
   SlaveVP        *slaveAssignedTo; //not valid until task animated
   BirthFnPtr      birthFn;         //This is the Fn executes as the task
   void           *initData;        //The data taken by the function
   LangMetaTaskFreer freer;
   bool32          goAheadAndFree;

   //NOTE: info needed for "wait" functionality is inside lang's metaTask
 };
//PRMetaTask -- prolog of every lang's meta task

struct _PRLangData
 { //============== First two must match PRCollElem ==============
   int32         langMagicNumber;
   PRLangData   *chainedLangData;
   //=============================================================
   LangDataFreer  freer;
   bool32         goAheadAndFree;
   SlaveVP       *slaveAssignedTo;
 };
//PRLangData -- this is the prolog of each lang's lang data

struct _PRCollElem
 {
   int32       hash;
   PRCollElem *chained;
 };
//PRCollElem -- this is generic form of all the prologs



//=========================  Extra Stuff Data Strucs  =======================
typedef struct
 {

 }
PRExcp; //exception


#endif	/* _PR__structs_H */

