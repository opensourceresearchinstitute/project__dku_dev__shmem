/*
 *  Copyright 2009 OpenSourceStewardshipFoundation.org
 *  Licensed under GNU General Public License version 2
 *
 * Author: seanhalle@yahoo.com
 * 
 */

/*NOTE: this file should only be included AFTER some other 
 * file has defined which kind of probe to use, and whether
 * probes are turned on or off..
 *
 *In other words, this is a static library that uses macros,
 * and there are multiple versions of the same macro.. which
 * version gets used depends on #define statements in a
 * DIFFERENT file..  (traditionally PR_defs__turn_on_and_off.h)
 *
 *So, this file relies on #defines that appear in other files,
 * which must come first in the sequence of #includes that 
 * include this one..
 */

#ifndef _PROBES_wrapper_library_H
#define	_PROBES_wrapper_library_H
#define _GNU_SOURCE

//=========================================================
//   Use only these aliases within application code
//=========================================================


#define PR_App__record_time_point_into_new_probe PR_WL__record_time_point_into_new_probe
#define PR_App__create_single_interval_probe   PR_WL__create_single_interval_probe
#define PR_App__create_histogram_probe         PR_WL__create_histogram_probe
#define PR_App__index_probe_by_its_name        PR_WL__index_probe_by_its_name
#define PR_App__get_probe_by_name              PR_WL__get_probe_by_name
#define PR_App__record_sched_choice_into_probe PR_WL__record_sched_choice_into_probe
#define PR_App__record_interval_start_in_probe PR_WL__record_interval_start_in_probe 
#define PR_App__record_interval_end_in_probe   PR_WL__record_interval_end_in_probe
#define PR_App__print_stats_of_probe           PR_WL__print_stats_of_probe
#define PR_App__print_stats_of_all_probes      PR_WL__print_stats_of_all_probes 

#endif	/* top ifndef */

