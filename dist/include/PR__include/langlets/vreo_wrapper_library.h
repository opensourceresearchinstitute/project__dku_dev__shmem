/*
 *  Copyright 2009 OpenSourceResearchInstitute.org
 *  Licensed under GNU General Public License version 2
 *
 * Author: seanhalle@yahoo.com
 *
 */

#ifndef _VREO_WRAPPER_H
#define	_VREO_WRAPPER_H

#include <PR__include/PR__structs__common.h>

   //uniquely identifies VSs -- should be a jenkins char-hash of "VSs" to int32
#define VReo_MAGIC_NUMBER 0000000003

//===========================================================================
typedef struct _VReoIsland VReoIsland;

typedef bool32 (*VReoCheckerFn ) ( VReoIsland * );
typedef void   (*VReoDoerFn )    ( VReoIsland * );
//===========================================================================

typedef struct
 {
   void          *buffer;
   bool32         portIsFull;
   SlaveVP       *waitingReaderVP;   //doubles as flag
   SlaveVP       *waitingWriterVP;
   
   void          *reader;  //either island or VP
   void          *writer;  //either island or VP
   
   int32          numReaderCheckerFns;  
   VReoCheckerFn *readerCheckerFns;     //checkers triggered when port state changes
   VReoDoerFn    *readerDoerFns;        //corresponding doer functions   

   int32          numWriterCheckerFns;  
   VReoCheckerFn *writerCheckerFns;     //checkers triggered when port state changes
   VReoDoerFn    *writerDoerFns;        //corresponding doer functions   
 }
VReoPort;

struct _VReoIsland
 {
   int32          numPorts;
   VReoPort     **ports;          //array of pointers to port structs
   
   int32          numCheckerFns;
   VReoCheckerFn *checkerFns;     //checkers triggered when state changes
   VReoDoerFn    *doerFns;        //corresponding doer functions   
   
   int32          lastCheckerToSucceed;
 };
//VReoIsland

typedef struct _VReoListElem VReoListElem;
 
struct _VReoListElem
 {
   void *payload;
   VReoListElem *next;
 };
//VReoListElem
 
typedef struct
 {
   int32        numPorts; //
   VReoPort    *ports;    //array of port structs
   VReoPort   **boundaryPorts;
   
   int32        numIslands;
   VReoIsland  *islands;  //array of island structs -- no pointers
   
   int32        numVPs;
   VReoListElem *VPs;
   
   int32        suspendScope; //given to PR -- VPs created suspended 
 }
VReoCircuit;

//Every application-defined birth param struct must have a pointer to a
// circuit as its first field.  An instance of one of those app-defined 
// structs is then cast to be a VReoBirthParams, in order for VReo to 
// access the circuit field without knowing anything about the app-specific
// part of the structure
typedef struct
 {
   VReoCircuit *circuit; 
 }
VReoBirthParams;
                 
//===========================================================================

//=======================

void
VReo__start( SlaveVP *seedVP );

void
VReo__shutdown( SlaveVP *seedVP );

void
VReo__wait_for_all_VReo_created_work_to_end( SlaveVP *seedVP );

//=======================

void
VReo__put_into_port( void *itemToPut, VReoPort *port, SlaveVP *callingVP );

void *
VReo__get_from_port( VReoPort *port, SlaveVP *callingVP );



SlaveVP *
VReo__create_VP( BirthFnPtr fnPtr, void *_params, 
                           VReoCircuit *circuit, SlaveVP *creatingVP );

void
VReo__end_VP( SlaveVP *VPToEnd );

//===========================================================================
#endif	/* _VReo_H */

