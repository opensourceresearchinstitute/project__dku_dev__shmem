/*
 *  Copyright 2009 OpenSourceResearchInstitute.org
 *  Licensed under GNU General Public License version 2
 *
 * Author: seanhalle@yahoo.com
 * 
 */

#ifndef _PR_INT_H
#define	_PR_INT_H
#define _GNU_SOURCE


#define LOCKED 1
#define UNLOCKED 0

/* MEANING OF   WL  PI  SS  int
 * These indicate which places the function is safe to use.  They stand for:
 * WL: Wrapper Library
 * PI: Plugin 
 * SS: Startup and Shutdown
 * int: internal to the PR implementation
 */

inline
void
PR_int__reset_slaveVP_to_BirthFn( SlaveVP *slaveVP, BirthFnPtr fnPtr,
                              void    *dataParam);

inline
void
PR_int__point_slaveVP_to_OneParamFn( SlaveVP *slaveVP, void *fnPtr,
                              void    *param);

inline
void
PR_int__point_slaveVP_to_TwoParamFn( SlaveVP *slaveVP, void *fnPtr,
                             void    *param1, void *param2);

//===========================================================================
//
//===========================================================================
inline
SlaveVP *
PR_int__create_slaveVP_helper( BirthFnPtr fnPtr, void *dataParam );

inline
SlaveVP *
PR_int__create_slaveVP( BirthFnPtr fnPtr, void *dataParam, PRProcess *process );

SlaveVP *
PR_int__get_recycled_slot_slave( );

SlaveVP *
PR_int__create_slot_slave( );

inline 
void
PR_int__replace_with_new_slot_slv( SlaveVP *slave );

void 
idle_fn(void* data, SlaveVP *animatingSlv);

inline
void
PR_int__put_task_into_slot( void *task, AnimSlot *slot );

inline
void
PR_int__put_slave_into_slot( SlaveVP *slave, AnimSlot *slot );

void inline
PRHandle__ServiceReq(  SlaveVP *requestingSlv );

void
PR_int__free_slaveVP( SlaveVP *slave );

void
PR_int__recycle_slaveVP( SlaveVP *slave );

void
freeLangDataAsElem( void *elem );

void
freeMetaTaskAsElem( void *elem );


//=============================
//=== Lange Env
//=
inline 
void *
PR_int__give_lang_env( PRLangEnv *protoLangEnv );

inline 
PRLangEnv *
PR_int__give_proto_lang_env( void *langEnv );

//inline
void *
PR_int__create_lang_env_in_process( int32 size, PRProcess *process, int32 magicNum );

inline
void *
PR_int__remove_lang_env_from_process_and_free( void *langEnv );

inline
void *
PR_int__give_lang_env_of_req( PRReqst *req, SlaveVP *requestingSlv );

inline
void *
PR_int__give_lang_env_for_slave( SlaveVP *slave, int32 magicNum );
//No WL version -- not safe!  if use env in WL, be sure data rd & wr is stable

inline
PRLangEnv *
PR_int__give_proto_lang_env_for_slave( SlaveVP *slave, int32 magicNumber );
//No WL version -- not safe!  if use env in WL, be sure data rd & wr is stable

inline
void *
PR_int__give_lang_env_from_process( PRProcess *process, int32 magicNum );
//No WL version -- not safe!  if use env in WL, be sure data rd & wr is stable

inline
PRLangEnv *
PR_int__give_proto_lang_env_from_process( PRProcess *process, int32 magicNum );

//=======================
//=== Meta Task
//=
inline 
PRMetaTask *
PR_int__give_prolog_of_lang_meta_task( void *task );

inline
void *
PR_int__give_lang_meta_task_of_prolog( PRMetaTask *metaTask);

inline
void *
PR_int__create_lang_meta_task( int32 size, LangMetaTaskFreer freer, int32 magicNum );

inline
void *
PR_int__create_lang_meta_task_in_slave( int32 size, LangMetaTaskFreer freer,
                                        SlaveVP *slave, int32 magicNum );

inline
PRMetaTask *
PR_int__create_generic_slave_meta_task( void *initData );

void
PR_int__free_lang_meta_task_and_remove_from_coll( void *langMetaTask );

inline
void
PR_int__insert_meta_task_into_slave( PRMetaTask *task, SlaveVP *slave );

inline
void
PR_int__insert_lang_meta_task_into_slave__ML( void *langMetaTask, SlaveVP *slave );

inline
void *
PR_int__give_lang_meta_task_from_slave( SlaveVP *slave, int32 magicNumer );

inline
SlaveVP *
PR_PI__give_slave_lang_meta_task_is_assigned_to( void *langMetaTask );

//==============
//=== Lang Data
//=
inline
void *
PR_PI__create_lang_data_in_slave( int32 size, LangDataFreer freer,
                                   SlaveVP *slave, int32 magicNum );

inline
void *
PR_int__give_lang_data_from_slave( SlaveVP *slave, int32 magicNumer );

inline
void *
PR_int__give_lang_data_of_prolog( PRLangData *langData);

//==================================================
//===  Collection
//=
PRCollElem ** //return an array of pointers
PR_int__make_collection_of_size( int32 numInColl );

inline
void
PR_int__insert_elem_into_collection( PRCollElem *elem, PRCollElem **coll, int32 hash );

inline
void
PR_int__replace_or_insert_elem_into_collection( PRCollElem *elem, 
                                                PRCollElem **coll, 
                                                int32 hash );

inline
void *
PR_int__lookup_elem_in_collection( int32 hash, PRCollElem **coll );

inline
void
PR_int__remove_elem_from_collection( int32 hash, PRCollElem **coll );

inline
void
PR_int__set_collection_to_empty( PRCollElem **coll );

inline
void
PR_int__apply_Fn_to_all_in_collection( void (*Fn)(void *), PRCollElem **coll );

//===========
//===
//=
void
PR_int__error( char *msgStr );

void
PR_int__throw_exception( char *msgStr, SlaveVP *reqstSlv, PRExcp *excpData );

/*Some macro magic -- the __FILE__ turns into a string, and the 
 * preprocessor merges the quotes.  But the __LINE__ turns into an int,
 * so have to use the # operator to turn it into a string..  but the
 * # operator doesn't expand what comes after, so have to do an extra
 * level so that pre-processor first expands __LINE__ into number then 
 * hands to the # operator, which turns into string (then it merges
 * quotes)
 */
#define PR__throw_simple_exception( throwingVP ) \
   do{ PR_int__throw_exception( __FILE__ ", " STR(__LINE__), throwingVP, NULL ); \
     } while(0)

#define STR( s ) #s

char *
PR_int__strDup( char *str );

inline void
PR_int__get_wrapper_lock();

inline void
PR_int__get_master_lock();

inline void
PR_int__get_malloc_lock();

#define PR_int__release_master_lock()  _PRTopEnv->masterLock  = UNLOCKED

#define PR_int__release_wrapper_lock() _PRTopEnv->wrapperLock = UNLOCKED

#define PR_int__release_malloc_lock()  _PRTopEnv->mallocLock  = UNLOCKED

inline uint32_t
PR_int__randomNumber();

inline void
PR_int__backoff_for_TooLongToGetLock( int32 numTriesToGetLock );

#endif	/* _PR_INT_H */

