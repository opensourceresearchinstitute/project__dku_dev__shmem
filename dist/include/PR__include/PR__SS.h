/*
 *  Copyright 2009 OpenSourceResearchInstitute.org
 *  Licensed under GNU General Public License version 2
 *
 * Author: seanhalle@yahoo.com
 * 
 */

#ifndef _PR__SS_H
#define	_PR__SS_H
#define _GNU_SOURCE

#include <PR__include/PR__structs__common.h>

//=========================  Function Prototypes  ===========================
/* MEANING OF   WL  PI  SS  int PROS
 * These indicate which places the function is safe to use.  They stand for:
 * 
 * WL   Wrapper Library -- wrapper lib code should only use these
 * PI   Plugin          -- plugin code should only use these
 * SS   Startup and Shutdown -- designates these relate to startup & shutdown
 * int32internal to PR -- should not be used in wrapper lib or plugin
 * PROS means "OS functions for applications to use"
 * 
 * PR_int__ functions touch internal PR data structs and are only safe
 *  to be used inside the master lock.  However, occasionally, they appear
 * in wrapper-lib or plugin code.  In those cases, very careful analysis
 * has been done to be sure no concurrency issues could arise.
 * 
 * PR_WL__ functions are all safe for use outside the master lock.
 * 
 * PROS are only safe for applications to use -- they're like a second
 * language mixed in -- but they can't be used inside plugin code, and
 * aren't meant for use in wrapper libraries, because they are themselves
 * wrapper-library calls!
 */

//===============  Startup and Shutdown  ================
//===
//=    Some of these are for PR internal use only, others for langlet use


#define \
PR_SS__malloc  PR_WL__malloc /*SS happens outside the Master*/
#define \
PR_SS__free    PR_WL__free

//===================
void *
PR_SS__create_lang_env( int32 size, SlaveVP *slave, int32 magicNum );

void
PR_SS__register_assigner( SlaveAssigner assigner, SlaveVP *seedVP, int32 magicNum );
void
PR_SS__register_lang_shutdown_handler( LangShutdownHdlr shutdownHdlr, SlaveVP *seedVP,
                                  int32 magicNum );
void
PR_SS__register_lang_data_creator( LangDataCreator langDataCreator, 
                                              SlaveVP *seedVP, int32 magicNum );
void
PR_SS__register_lang_meta_task_creator( LangDataCreator langMetaTaskCreator, 
                                              SlaveVP *seedVP, int32 magicNum );
void
PR_SS__register_make_slave_ready_fn( MakeSlaveReadyFn fn, SlaveVP *seedVP,
                                                               int32 magicNum );
void
PR_SS__register_make_task_ready_fn( MakeTaskReadyFn fn, SlaveVP *seedVP,
                                    int32 magicNum );


//================================
//===
//=       PR internal use only
void
PR_SS__create_topEnv();

AnimSlot *
PR_SS__create_anim_slot( int32 coreSlotsAreOn );

void
PR_SS__create_the_coreCtlr_OS_threads();

int
PR_SS__give_num_cores();

//===================

void
PR_SS__end_process_normally( PRProcess *process );

void
PR_SS__shutdown_OS_threads();

SlaveVP* 
PR_SS__create_shutdown_slave();

void
PR_SS__cleanup_at_end_of_shutdown();
 
void
PR_SS__print_out_measurements();

void
PR_SS__wait_for_PR_to_shutdown();

//=============================
//===
//=

#define \
PR_SS__give_lang_data_from_slave   PR_int__give_lang_data_from_slave
#define \
PR_SS__give_lang_meta_task_from_slave   PR_int__give_lang_meta_task_from_slave
#define \
PR_SS__give_proto_lang_env_for_slave   PR_int__give_proto_lang_env_for_slave
#define \
PR_SS__give_lang_env_for_slave   PR_int__give_lang_env_for_slave
#define \
PR_SS__give_lang_env_from_process   PR_int__give_lang_env_from_process

#define \
PR_SS__malloc  PR_WL__malloc /*SS happens outside the Master*/
#define \
PR_SS__free    PR_WL__free

//================================================
#endif	/*  */

